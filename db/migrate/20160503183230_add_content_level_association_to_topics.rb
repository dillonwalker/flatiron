class AddContentLevelAssociationToTopics < ActiveRecord::Migration
  def self.up
	  add_column :topics, :content_level_id, :integer
      add_index 'topics', ['content_level_id'], :name => 'index_content_level_id' 
  end

  def self.down
	  remove_column :topics, :content_level_id
  end
end
