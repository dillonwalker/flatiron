class AddTopicsAssociationToContentType < ActiveRecord::Migration
  def self.up
      add_column :topics, :content_type_id, :integer
      add_index 'topics', ['content_type_id'], :name => 'index_content_type_id' 
  end

  def self.down
	  remove_column :topics, :content_type_id
  end
end
