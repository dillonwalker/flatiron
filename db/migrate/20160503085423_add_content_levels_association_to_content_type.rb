class AddContentLevelsAssociationToContentType < ActiveRecord::Migration
  def self.up
	  add_column :content_levels, :content_type_level_id, :integer
	  add_index 'content_levels', ['content_type_level_id'], :name => 'index_content_type_level_id' 
  end

  def self.down
	  remove_column :content_levels, :content_type_level_id
  end
end
