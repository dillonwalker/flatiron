class CreateContentLevels < ActiveRecord::Migration
  def change
    create_table :content_levels do |t|
      t.string :name
      t.integer :level
      t.text :shorted

      t.timestamps null: false
    end
  end
end
