class AddColorToContentType < ActiveRecord::Migration
  def change
    add_column :content_types, :color, :string
  end
end
