# README #

Flatiron is a Content Management tool built for intelligent information storage. The interface allows any technical skill level to interact with the database, and users to interact with the appropriate data through filtering mechanisms.

Flatiron also has a robust API for automated storage and use with other services.

### How do I get set up? ###

* Clone the repo
* Run bundler to get your app set up (bundle install)
* By default, the app is configured to use SQLite3; adjust your database as desired
* Set up your schema with Rake (rake db:schema:load)
* Light it up (rails server)