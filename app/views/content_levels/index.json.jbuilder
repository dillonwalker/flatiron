json.array!(@content_levels) do |content_level|
  json.extract! content_level, :id, :name, :level, :shorted, :content_type_level_id
  json.url content_level_url(content_level, format: :json)
end
