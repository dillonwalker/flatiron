class DashboardController < ApplicationController
	
	def index
		@content_types = ContentType.all
		if user_signed_in?
			render 'index'
		else 
			render 'explorer'
		end
	end
	
	def explorer
		@content_types = ContentType.all
		if session[:user_level_setting].nil?
			render '_set_user_level'
		end
		respond_to do |format|               
			format.html {render :explorer}
			format.json {
				if params[:level]
					level = ContentLevel.find_by level: params[:level]
					render json: Topic.where(content_level_id: level.id).take
					return
				end
				render json: Topic.all }
		end
	end
	
	def change_user_level
		respond_to do |format|               
			format.js
		end
	end
end