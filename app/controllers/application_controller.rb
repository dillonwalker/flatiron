class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
	before_action :set_vars
	
	def set_vars
	end
	
	private
	
	def check_sign_in
		if !user_signed_in?
			redirect_to '/users/sign_in'
		end
	end
	
	def check_user_level
		if user_signed_in?
			session[:user_level_setting] = provide_user_level
		end
	end
end
