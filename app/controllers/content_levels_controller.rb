class ContentLevelsController < ApplicationController
  before_action :set_content_level, only: [:show, :edit, :update, :destroy]

  # GET /content_levels
  # GET /content_levels.json
  def index
    @content_levels = ContentLevel.all.order(level: 'ASC')
  end

  # GET /content_levels/1
  # GET /content_levels/1.json
  def show
  end

  # GET /content_levels/new
  def new
	unless params[:content_type_id].nil?
		@content_type = ContentType.find(params[:content_type_id])
	end
    @content_level = ContentLevel.new
  end

  # GET /content_levels/1/edit
  def edit
	unless params[:content_type_id].nil?
		@content_type = ContentType.find(params[:content_type_id])
	end
  end

  # POST /content_levels
  # POST /content_levels.json
  def create
    @content_level = ContentLevel.new(content_level_params)

    respond_to do |format|
      if @content_level.save
        format.html { redirect_to @content_level, notice: 'Content level was successfully created.' }
        format.json { render :show, status: :created, location: @content_level }
      else
        format.html { render :new }
        format.json { render json: @content_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /content_levels/1
  # PATCH/PUT /content_levels/1.json
  def update
    respond_to do |format|
      if @content_level.update(content_level_params)
		 format.html { redirect_to content_levels_path, notice: 'Content level was successfully updated.' }
        format.json { render :show, status: :ok, location: @content_level }
      else
        format.html { render :edit }
        format.json { render json: @content_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /content_levels/1
  # DELETE /content_levels/1.json
  def destroy
    @content_level.destroy
    respond_to do |format|
      format.html { redirect_to content_levels_url, notice: 'Content level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
	
	def set_user_level_setting
		session[:user_level_setting] = params[:user_level][:content_level_id]
		puts session[:user_level_setting]
		redirect_to explorer_path, notice: "#UserLevelIDSet: #{session[:user_level_setting].inspect}"
	end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content_level
      @content_level = ContentLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def content_level_params
      params.require(:content_level).permit(:name, :level, :shorted, :content_type_level_id)
    end
end
