class Topic < ActiveRecord::Base
	belongs_to :content_type
	has_one :content_level
end
