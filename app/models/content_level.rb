class ContentLevel < ActiveRecord::Base
	belongs_to :content_type
	has_many :topics
end
