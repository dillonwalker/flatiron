class ContentType < ActiveRecord::Base
	has_many :content_levels
	has_many :topics
end
