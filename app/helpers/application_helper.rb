module ApplicationHelper
	
	def provide_user_level
		unless session[:user_level_setting].nil?
			return session[:user_level_setting]
		end
		if user_signed_in?
			return current_user.content_level_setting
		else 
			return 1
		end
	end
	
	def provide_available_levels
      levels = ContentLevel.all.order(level: 'ASC')
		return levels
	end
end
