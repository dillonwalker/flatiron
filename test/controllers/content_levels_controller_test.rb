require 'test_helper'

class ContentLevelsControllerTest < ActionController::TestCase
  setup do
    @content_level = content_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:content_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create content_level" do
    assert_difference('ContentLevel.count') do
      post :create, content_level: { level: @content_level.level, name: @content_level.name, shorted: @content_level.shorted }
    end

    assert_redirected_to content_level_path(assigns(:content_level))
  end

  test "should show content_level" do
    get :show, id: @content_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @content_level
    assert_response :success
  end

  test "should update content_level" do
    patch :update, id: @content_level, content_level: { level: @content_level.level, name: @content_level.name, shorted: @content_level.shorted }
    assert_redirected_to content_level_path(assigns(:content_level))
  end

  test "should destroy content_level" do
    assert_difference('ContentLevel.count', -1) do
      delete :destroy, id: @content_level
    end

    assert_redirected_to content_levels_path
  end
end
