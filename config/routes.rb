Rails.application.routes.draw do
  resources :topics
  resources :content_levels
  resources :content_types
  root 'dashboard#index'
  
  devise_for :users
  
	get 'explorer' => 'dashboard#explorer'
	get 'change_user_level' => 'dashboard#change_user_level'
	
	post 'set_user_level' => 'content_levels#set_user_level_setting', as: 'set_user_level'
  
  

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
